import React from 'react';
import 'antd/dist/antd.css';
import Calculator from './module/Calculator/Calculator';
import CalculatorDate from './module/CalculatorDate/CalculatorDate';

function App() {
  return (
    <div className="container">
      <Calculator />
      <CalculatorDate />
    </div>
  );
}

export default App;
