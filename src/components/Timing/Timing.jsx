import React from 'react';
import { TimePicker } from 'antd';
import { IoIosAddCircleOutline, IoIosRemoveCircleOutline } from "react-icons/io";
import moment from 'moment';

import './Timing.scss';
import "react-datepicker/dist/react-datepicker.css";

class Timing extends React.Component {

    state = {
        count: 3
    };

    onDecrease = () => {
        this.setState({ 
            count: this.state.count - 1 
        });
    }
    
    onIncrease = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    render() {
        const formatTime = 'HH:mm';
        const formatDate = 'MM-DD-YYYY';

        return(
            <div className="timing">
                <div className="date-time">

                    <TimePicker 
                        style={{ width: "48.5%" }}
                        defaultValue={moment('12-25-1995', formatDate )}
                        format={formatDate} 
                    />

                    <TimePicker 
                        style={{ width: "48.5%", marginLeft: "3%" }}
                        defaultValue={moment('19:03', formatTime)} 
                        format={formatTime} 
                    />

                </div>
                <div className="deadline">
                    <div className="button-timing">
                        <IoIosRemoveCircleOutline color='#bfc8d4' onClick={this.onDecrease} />
                    </div>
                    <span>{this.state.count} hours to deadline</span>
                    <div className="button-timing">
                        <IoIosAddCircleOutline color='#377fea' onClick={this.onIncrease} />
                    </div>
                </div>
            </div>
        );
    }
}

export default Timing;