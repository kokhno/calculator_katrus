import React from 'react';
import { IoIosAddCircleOutline, IoIosRemoveCircleOutline } from "react-icons/io";
import { Select, Icon} from 'antd';

import './TypePaper.scss';

class TypePaper extends React.Component {

    state = {
        page: 3,
        words: 280
    }

    onDecrease = () => {
        this.setState({
            page: this.state.page - 1,
            words: this.state.words - 30
        })
    }

    onIncrease = () => {
        this.setState({
            page: this.state.page + 1,
            words: this.state.words + 30
        })
    }

    render() {
      const { Option } = Select;
      return(
          <div className="type-paper">
              <div className="paper-select">
                <Select 
                    className="select-label"
                    defaultValue="Admission Essay / Application"
                    suffixIcon= {<Icon type="arrow-down" />}
                >
                    <Option value="essay">Essay</Option>
                    <Option value="team">Term Paper</Option>
                    <Option value="dissertation">Dissertation & Thesis</Option>
                    <Option value="citation">Citation Styles</Option>
                    <Option value="essay-2">Essay</Option>
                </Select>
            </div>
            <div className="page">
                <div className="page-words select-label">
                    <span>{this.state.page} page = {this.state.words} words</span>
                    <div className="button-group">
                        <IoIosRemoveCircleOutline color='#bfc8d4' onClick={this.onDecrease} />
                        <IoIosAddCircleOutline 
                            color='#377fea' 
                            style={{ marginLeft: "5px" }} 
                            onClick={this.onIncrease}
                        />
                    </div>
                </div>
            </div>
          </div>
      );
    }
}

export default TypePaper;