import React from 'react';
import { Menu } from 'antd';

import './Menu.scss';

class MenuNavigation extends React.Component {

    state = {
        current: "freshman"
    }

    handleClick = e => {
        this.setState({
            current: e.key,
        });
    };

    render() {
        return(
            <div className="menu">
                <Menu 
                    onClick={this.handleClick} 
                    selectedKeys={[this.state.current]} 
                    mode="horizontal"
                >
                    <Menu.Item key="high-school">
                        High School
                    </Menu.Item>
                    <Menu.Item key="freshman">
                        College / University-Freshman
                    </Menu.Item>
                    <Menu.Item key="sophomore">
                        College / University - Sophomore
                    </Menu.Item>
                    <Menu.Item key="junior">
                        College / University - Junior 
                    </Menu.Item>
                    <Menu.Item key="senior">
                        College / University - Senior
                    </Menu.Item>
                </Menu>
            </div>
        );
    }
}

export default MenuNavigation;