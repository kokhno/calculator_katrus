import React from 'react';

import './Header.scss';

const Header = (props) => {

    return (
        <div className="header">
            <h2 className="header-name">{props.headerName}</h2>
            <span className="header-switch">
                <strong>Custom due date</strong> <input type="checkbox" />
            </span>
        </div>
    );
}

export default Header;
