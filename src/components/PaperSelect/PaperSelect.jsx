import React from 'react';
import { Select, Icon } from 'antd';

import './PaperSelect.scss';

class PaperSelect extends React.Component {

    render() {
      
      const { Option } = Select;

      return(
          <div className="paper-select">
              <Select 
                defaultValue="Admission Essay / Application" 
                className="select-label-top"
                suffixIcon= {<Icon type="arrow-down" />}
              >
                {this.props.PaperSelect.map(paper => (
                  <Option key={paper.id} value={paper.value}>
                    {paper.name}
                  </Option>
                ))}
              </Select>
          </div>
      );
    }
}

export default PaperSelect;