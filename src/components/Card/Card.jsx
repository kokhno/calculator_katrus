import React from "react";
import Slider from "react-slick";

import './Card.scss';

class Card extends React.Component {
  render() {    
    const settings = {
        focusOnSelect: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 800,
        variableWidth: true,
        centerMode: true,
        useCSS: true
      };
    return (
      <Slider {...settings} className="card-slider">
        <div>
            <div className="content">
                <span>27 nov</span>
                <h5>12 hours</h5>
                <h5>$38.76</h5>
                <span>Page</span>
            </div>
        </div>
        <div>
            <div className="content">
                <span>27 nov</span>
                <h5>12 hours</h5>
                <h5>$38.76</h5>
                <span>Page</span>
          </div>
        </div>
        <div className="active">
            <div className="content">
                <span>27 nov</span>
                <h5>12 hours</h5>
                <h5>$38.76</h5>
                <span>Page</span>
            </div>
        </div>
        <div>
            <div className="content">
                <span>27 nov</span>
                <h5>12 hours</h5>
                <h5>$38.76</h5>
                <span>Page</span>
            </div>
        </div>
        <div>
            <div className="content">
                <span>27 nov</span>
                <h5>12 hours</h5>
                <h5>$38.76</h5>
                <span>Page</span>
            </div>
        </div>
      </Slider>
    );
  }
}

export default Card;