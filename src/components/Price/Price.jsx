import React from 'react';
import { Button } from 'antd';

import './Price.scss';

class Price extends React.Component {

    state = {
        date: new Date(),
        loading: false
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

        tick() {
        this.setState({
            date: new Date()
        });
    }

    handleOk = (e) => {
        e.preventDefault();
        this.setState({ loading: true });

        setTimeout(() => {
            this.setState({ loading: false });
        }, 1000)
    }

    render() {
        const optionDate = { weekday: 'long', month: 'short', 
            day: 'numeric', hour: 'numeric', minute: 'numeric'};
        const { loading } = this.state;

        return(
            <div className="price">
                <div className="price-title">
                    <div className="time-now">
                        {this.state.date.toLocaleDateString('ua-UA', optionDate)}
                    </div>
                    <div className="total-price">
                        <span>${this.props.price}</span>
                    </div>
                </div>
                <Button 
                    className="button-price"
                    loading={loading}
                    onClick={this.handleOk}
                >
                    Order now
                </Button>
            </div>
        );
    }
}

export default Price;