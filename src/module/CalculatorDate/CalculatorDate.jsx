import React from 'react';
import { Row, Col } from 'antd';
import Header from '../../components/Header/Header';
import MenuNavigation from '../../components/Menu/Menu';
import Price from '../../components/Price/Price';
import TypePaper from '../../components/TypePaper/TypePaper';
import Timing from '../../components/Timing/Timing';

const CalculatorDate = () => {
    
    return(
        <div className="calculator-date">
            <Header headerName="Calculator date" />
            <MenuNavigation />
            <Row gutter={[8, 8]}>
                <Col span={8} >
                    <Col span={24}>
                        <span className="desc">Type of paper needed</span>
                    </Col>
                    <TypePaper />
                </Col>    
                <Col span={12}>
                    <Col span={12}>
                        <span className="desc">Date</span>
                    </Col>
                    <Col span={12}>
                        <span className="desc-time">Time</span>
                    </Col>
                    <Timing />
                </Col>    
                <Col span={4}>
                    <Price price="61.95" />
                </Col>
            </Row>
        </div>
    );
}

export default CalculatorDate;