import React from 'react';
import { Row, Col } from 'antd';
import Header from '../../components/Header/Header';
import MenuNavigation from '../../components/Menu/Menu';
import PaperSelect from '../../components/PaperSelect/PaperSelect';
import Price from '../../components/Price/Price';
import Card from '../../components/Card/Card';

const Calculator = () => {

    const Paper = [
        {'id': 1, 'name': 'Essay', 'value': 'essay'},
        {'id': 2, 'name': 'Term Paper', 'value': 'team'},
        {'id': 3, 'name': 'Dissertation & Thesis', 'value': 'dissertation'},
        {'id': 4, 'name': 'Citation Styles', 'value': 'citation'},
        {'id': 5, 'name': 'Essay', 'value': 'essay-2'},
    ]
    
    return(
        <div className="calculator" style={{ marginBottom: "10vh" }}>
            <Header headerName="Calculator" />
            <MenuNavigation />
            <Row>
                <Col span={8} >
                    <Col span={24}>
                        <span className="desc">Type of paper needed</span>
                    </Col>
                    <PaperSelect PaperSelect={Paper} />
                </Col>    
                <Col span={12}>
                    <Col span={24}>
                        <span className="desc">Pages</span>
                    </Col>
                    <Card />
                </Col>    
                <Col span={4}>
                    <Price price="61.95" />
                </Col>
            </Row>
        </div>
    );
}

export default Calculator;